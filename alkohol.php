<h1>Aleks' Alkoholformel for hyttetur&reg;&copy;&trade;</h1>
<form id='alcohol'>
<label for="days">Hvor mange dager skal du på hyttetur?</label><br/>
<input required="required" type="number" id="days" name="days" placeholder="Antall dager"/><br/>
<label for="units">Hvor mange alkoholenheter trenger du på en normal kveld?</label><br/>
<input required="required" type="number" id="units" name="units" placeholder="Antall enheter"/><br />
<button name="calculate" id="calculate" type="submit">Beregn</button>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$("#alcohol").submit(calculate);
		$("#alcohol_oneday").submit(calculate_oneday);
	});

	calculate = function() {
		var days = parseInt($("#days").val());
		var units = parseInt($("#units").val());
		var youNeed = days*units;
		for(var i = 1; i < days; i++) {
			youNeed = youNeed*2;
		}
		ShowMessage("Du skal på hyttetur i " + days + " dager. Til dette trenger du " + youNeed + " enheter med alkohol.");
		return false;
	};

</script>